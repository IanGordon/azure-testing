ian@Azure:~/testing$ az group create --name myResourceGroup --location westeurope
{
  "id": "/subscriptions/a3fb0b20-15da-4871-a021-1b90f1f25419/resourceGroups/myResourceGroup",
  "location": "westeurope",
  "managedBy": null,
  "name": "myResourceGroup",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null
}
ian@Azure:~/testing$

ian@Azure:~/testing$ az network vnet create -g myResourceGroup -n myVnet --address-prefix 10.0.0.0/16 --subnet-name mySubnet --subnet-prefix 10.0.1.0/24
{
  "newVNet": {
    "additionalProperties": {},
    "addressSpace": {
      "additionalProperties": {},
      "addressPrefixes": [
        "10.0.0.0/16"
      ]
    },
    "ddosProtectionPlan": null,
    "dhcpOptions": {
      "additionalProperties": {},
      "dnsServers": []
    },
    "enableDdosProtection": false,
    "enableVmProtection": false,
    "etag": "W/\"0e8dae9f-19a5-42f9-88bc-90ea197c6a5c\"",
    "id": "/subscriptions/a3fb0b20-15da-4871-a021-1b90f1f25419/resourceGroups/myResourceGroup/providers/Microsoft.Network/virtualNetworks/myVnet",
    "location": "westeurope",
    "name": "myVnet",
    "provisioningState": "Succeeded",
    "resourceGroup": "myResourceGroup",
    "resourceGuid": "5aceafc2-c076-4148-80a2-948d48a7b3d5",
    "subnets": [
      {
        "additionalProperties": {},
        "addressPrefix": "10.0.1.0/24",
        "etag": "W/\"0e8dae9f-19a5-42f9-88bc-90ea197c6a5c\"",
        "id": "/subscriptions/a3fb0b20-15da-4871-a021-1b90f1f25419/resourceGroups/myResourceGroup/providers/Microsoft.Network/virtualNetworks/myVnet/subnets/mySubnet",
        "ipConfigurations": null,
        "name": "mySubnet",
        "networkSecurityGroup": null,
        "provisioningState": "Succeeded",
        "resourceGroup": "myResourceGroup",
        "resourceNavigationLinks": null,
        "routeTable": null,
        "serviceEndpoints": null
      }
    ],
    "tags": {},
    "type": "Microsoft.Network/virtualNetworks",
    "virtualNetworkPeerings": []
  }
}
ian@Azure:~/testing$

check out below for the template the .json is based on
https://docs.microsoft.com/en-us/azure/virtual-machines/linux/create-ssh-secured-vm-from-template